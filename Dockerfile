FROM openjdk:11
ADD target/acme.jar acme.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "acme.jar"]