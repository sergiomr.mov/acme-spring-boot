# Proyecto Acme

Prueba de código de una API REST creada con Spring Boot

## Lanzar con Docker

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas mediante contenedores dockers.

### Pre-requisitos

* Realizar pull del proyecto o descargarlo en local.
* Tener docker instalado en local
* Tener maven instalado en local

### Pasos

Preparamos el contenedor de la base de datos:

```
docker pull mysql:5.7
docker run --name mysql-standalone -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=acme -e MYSQL_USER=acme -e MYSQL_PASSWORD=acme -d mysql:5.7

```

Compilamos la aplicación y comprobamos que no hay errores en los test:

```
mvn install
```

Desplegamos la aplicación en un nuevo contenedor:

```
docker build -t acme .
docker run -d -p 8089:8089 --name acme --link mysql-standalone:mysql acme
```

La aplicación tarda unos segundo en arrancar. Para comprobar el despliegue utilizamos:
```
docker logs acme
```


## Lanzar con Spring 

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas mediante contenedores el lanzador de Spring.

### Pre-requisitos

* Realizar pull del proyecto o descargarlo en local
* Tener docker instalado en local
* Tener maven instalado en local
* Tener una un servidor de base de datos MySql

### Configuración

El necesario crear un nuevo esquema con el nombre "acme" en la base de datos y configurar el archivo application.preoperties con los datos de conexión.

```
spring.datasource.url = jdbc:mysql://localhost:3306/acme?useSSL=false
spring.datasource.username = user
spring.datasource.password = pass
```

### Pasos
Desplegamos la aplicación utilizando el lanzador de Spring Boot:

```
./mvnw spring-boot:run
```

## LLamadas API REST

Las llamadas a la API se pueden obtener utilizando la [Documentación de Postman](https://documenter.getpostman.com/view/11370694/TVenf8ZV)


---
Creado con la plantilla de [Villanuevand](https://github.com/Villanuevand)