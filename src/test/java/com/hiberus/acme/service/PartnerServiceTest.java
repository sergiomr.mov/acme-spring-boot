package com.hiberus.acme.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import com.hiberus.acme.partners.dao.PartnerDao;
import com.hiberus.acme.partners.dao.SloganDao;
import com.hiberus.acme.partners.model.Partner;
import com.hiberus.acme.partners.model.Slogan;
import com.hiberus.acme.partners.service.PartnerService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;

@SpringBootTest
public class PartnerServiceTest {
	
	@Autowired
	private PartnerService partnerService;

  @MockBean
	private PartnerDao partnerDao;

	@MockBean
	private SloganDao sloganDao;

	@Test
	public void listTest() {
			when(partnerDao.findAll()).thenReturn(Collections.singletonList(new Partner()));
			assertThat(partnerService.list()).isNotEmpty();
	}

	@Test
	public void listIdTest() {
		Partner partner = new Partner();
		partner.setIdPartner(1L);
		when(partnerDao.findById(Mockito.anyLong())).thenReturn(Optional.of(partner));
		when(sloganDao.findByPartner(Mockito.any(Partner.class))).thenReturn(Collections.singletonList(new Slogan()));
		assertThat(partnerService.listId(1L)).isNotNull();
	}

	@Test
	public void listIdErrorTest() {
		Partner partner = new Partner();
		partner.setIdPartner(1L);
		when(partnerDao.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		assertThrows(DataIntegrityViolationException.class, () -> {
			partnerService.listId(1L);
		});
	}

	@Test
	public void saveTest() {
		Partner partner = new Partner();
		partner.setEmail("ejemplo@ejemplo.com");
		when(partnerDao.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		when(partnerDao.save(Mockito.any(Partner.class))).thenReturn(partner);
		assertEquals(partner, partnerService.save(partner));
	}

	@Test
	public void saveErrorTest() {
		Partner partner = new Partner();
		partner.setEmail("ejemplo@ejemplo.com");
		when(partnerDao.findByEmail(Mockito.anyString())).thenReturn(Optional.of(new Partner()));
		assertThrows(DataIntegrityViolationException.class, () -> {
			partnerService.save(partner);
		});
	}

	@Test
	public void updateTest() {
		Partner partner = new Partner();
		partner.setEmail("ejemplo@ejemplo.com");
		when(partnerDao.findByEmail(Mockito.anyString())).thenReturn(Optional.of(partner));
		when(partnerDao.save(Mockito.any(Partner.class))).thenReturn(partner);
		assertEquals(partner, partnerService.update(partner));
	}

	@Test
	public void updateErrorTest() {
		Partner partner = new Partner();
		partner.setEmail("ejemplo@ejemplo.com");
		when(partnerDao.findByEmail(Mockito.anyString())).thenReturn(Optional.empty());
		assertThrows(DataIntegrityViolationException.class, () -> {
			partnerService.update(partner);
		});
	}
	
}
