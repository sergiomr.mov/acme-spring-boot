package com.hiberus.acme.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.hiberus.acme.partners.dao.PartnerDao;
import com.hiberus.acme.partners.dao.SloganDao;
import com.hiberus.acme.partners.model.Partner;
import com.hiberus.acme.partners.model.Slogan;
import com.hiberus.acme.partners.service.SloganService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;

@SpringBootTest
public class SloganServiceTest {
	
	@Autowired
	private SloganService sloganService;

  @MockBean
	private PartnerDao partnerDao;

	@MockBean
	private SloganDao sloganDao;
	
	@Test
	public void findByPartnerTest() {
		Partner partner = new Partner();
		partner.setIdPartner(1L);
		when(sloganDao.findByPartner(Mockito.any(Partner.class))).thenReturn(Collections.singletonList(new Slogan()));
		assertThat(sloganService.findByPartner(partner)).isNotEmpty();
	}

	@Test
	public void deleteTest() {
		Slogan slogan = new Slogan();
		slogan.setIdSlogan(1L);
		when(sloganDao.findById(Mockito.anyLong())).thenReturn(Optional.of(slogan));
		sloganService.delete(1L);
		verify(sloganDao).delete(slogan);
	}

	@Test
	public void deleteErrorTest() {
		when(sloganDao.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		assertThrows(DataIntegrityViolationException.class, () -> {
			sloganService.delete(1L);
		});
	}

	@Test
	public void saveTest() {

		Slogan slogan = new Slogan();
		slogan.setIdSlogan(1L);
	
		Partner partner = new Partner();
		partner.setEmail("ejemplo@ejemplo.com");
		slogan.setPartner(partner);

		when(partnerDao.findById(Mockito.anyLong())).thenReturn(Optional.of(partner));
		when(sloganDao.findByPartner(Mockito.any(Partner.class))).thenReturn(Collections.singletonList(new Slogan()));
		when(sloganDao.save(Mockito.any(Slogan.class))).thenReturn(slogan);
		assertThat(sloganService.save(slogan,1L)).isNotNull();

	}

	@Test
	public void saveNoPartnerErrorTest() {

		Slogan slogan = new Slogan();
		slogan.setIdSlogan(1L);
		
		when(partnerDao.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		assertThrows(DataIntegrityViolationException.class, () -> {
			sloganService.delete(1L);
		}, "No se ha encontrado el registro de Partner");

	}

	@Test
	public void saveMaxSloganErrorTest() {

		Slogan slogan = new Slogan();
		slogan.setIdSlogan(1L);

		List<Slogan> slogans = List.of(new Slogan(), new Slogan(), new Slogan());
		
		when(partnerDao.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		when(sloganDao.findByPartner(Mockito.any(Partner.class))).thenReturn(slogans);
		assertThrows(DataIntegrityViolationException.class, () -> {
			sloganService.delete(1L);
		}, "Se ha alcanzado el máximo número de Slogans");

	}

}
