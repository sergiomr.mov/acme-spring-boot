package com.hiberus.acme.partners.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.hiberus.acme.partners.model.Partner;
import com.hiberus.acme.partners.service.PartnerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PartnerController {
	
	@Autowired
	private PartnerService partnerService;

	@GetMapping("/partners")
	public Iterable<Partner> getAllPartners() {
		return partnerService.list();
	}

	@PostMapping("/partners/save")
	public Partner save(@Valid @RequestBody Partner partner) {
		return partnerService.save(partner);
	}

	@GetMapping("/partners/{id}")
	public Partner listPersonById(@PathVariable("id") Long id) {
		return partnerService.listId(id);
	}

	@PutMapping("/partners/{id}")
	public Partner updatePerson(@PathVariable("id") Long id, @Valid @RequestBody Partner partner) {
		partner.setIdPartner(id);
		return partnerService.update(partner);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
				String errorMessage = error.getDefaultMessage();
				errors.put("error", errorMessage);
		});
		return errors;
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public Map<String, String> duplicateEmailException(HttpServletRequest req, DataIntegrityViolationException e) {
		Map<String, String> errors = new HashMap<>();
		String errorMessage = e.getMessage();
		errors.put("error", errorMessage);
		return errors;
	}

}
