package com.hiberus.acme.partners.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.hiberus.acme.partners.model.Slogan;
import com.hiberus.acme.partners.service.SloganService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SloganController {

	@Autowired
	private SloganService sloganService;

	@PostMapping("/partners/{id}/slogans")
	public Slogan saveSlogan(@PathVariable("id") Long id, @Valid @RequestBody Slogan slogan) {
		return sloganService.save(slogan, id);
	}

	@DeleteMapping("/partners/slogans/{id}")
	public void deleteSlogan(@PathVariable("id") Long id) {
		sloganService.delete(id);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
				String errorMessage = error.getDefaultMessage();
				errors.put("error", errorMessage);
		});
		return errors;
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public Map<String, String> duplicateEmailException(HttpServletRequest req, DataIntegrityViolationException e) {
		Map<String, String> errors = new HashMap<>();
		String errorMessage = e.getMessage();
		errors.put("error", errorMessage);
		return errors;
	}

}
