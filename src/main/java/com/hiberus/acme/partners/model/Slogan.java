package com.hiberus.acme.partners.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Slogan {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idSlogan")
	private Long idSlogan;

	@Column(name = "title", nullable = false, length = 70)
	private String title;

	private String text;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_partner")
	private Partner partner;

	public Long getIdSlogan() {
		return idSlogan;
	}

	public void setIdSlogan(Long idSlogan) {
		this.idSlogan = idSlogan;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

}