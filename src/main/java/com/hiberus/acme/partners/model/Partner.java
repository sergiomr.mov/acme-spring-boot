package com.hiberus.acme.partners.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Partner {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idPartner")
	private Long idPartner;

	@Column(name = "firstname", nullable = false, length = 70)
	@NotBlank(message = "Nombre es obligatorio")
	private String name;

	@Column(name = "lastname", nullable = false, length = 70)
	@NotBlank(message = "Apellido es obligatorio")
	private String lastName;

	@Column(name = "adress", length = 200)
	private String adress;

	@Column(name = "city", length = 70)
	private String city;

	@Column(name = "email", length = 200, unique=true)
	@NotBlank(message = "Email es obligatorio")
	private String email;

	@JsonManagedReference
	@Transient
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy="partner")
	private List<Slogan> slogan = new ArrayList<>();

	public Partner(){}

	public Partner(Long idPartner, String name, String lastName, String adress, String city, String email) {
		this.idPartner = idPartner;
		this.name = name;
		this.lastName = lastName;
		this.adress = adress;
		this.city = city;
		this.email = email;
	}

	public Long getIdPartner() {
		return idPartner;
	}

	public void setIdPartner(Long idPartner) {
		this.idPartner = idPartner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Slogan> getSlogan() {
		return slogan;
	}

	public void setSlogan(List<Slogan> slogan) {
		this.slogan = slogan;
	}

	public void addSlogan(Slogan slogan){
		this.slogan.add(slogan);
	}

}