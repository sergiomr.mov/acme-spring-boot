package com.hiberus.acme.partners.dao;

import java.util.List;

import com.hiberus.acme.partners.model.Partner;
import com.hiberus.acme.partners.model.Slogan;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SloganDao extends CrudRepository<Slogan, Long> {
	List<Slogan> findByPartner(Partner partner);
}
