package com.hiberus.acme.partners.dao;

import java.util.Optional;

import com.hiberus.acme.partners.model.Partner;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartnerDao extends CrudRepository<Partner, Long> {

	public Optional<Partner> findByEmail(String email);

}
