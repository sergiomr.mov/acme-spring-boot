package com.hiberus.acme.partners.service;

import java.util.Optional;

import com.hiberus.acme.partners.dao.PartnerDao;
import com.hiberus.acme.partners.dao.SloganDao;
import com.hiberus.acme.partners.model.Partner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class PartnerService {

	@Autowired
	private PartnerDao partnerDao;

	@Autowired
	private SloganDao sloganDao;

	public Partner save(Partner partner) { 

		Optional<Partner> partnerOptional = partnerDao.findByEmail(partner.getEmail());
		if(partnerOptional.isPresent()){
			throw new  DataIntegrityViolationException("El email ya existe");
		}

		return partnerDao.save(partner); 

	}

	public Partner update(Partner partner) { 

		Optional<Partner> partnerOptional = partnerDao.findByEmail(partner.getEmail());
		Partner partnerToUpdate = partnerOptional.orElseThrow(() -> new DataIntegrityViolationException("No se ha encontrado el registro de Partner"));
		partnerToUpdate.setName(partner.getName());
		partnerToUpdate.setLastName(partner.getLastName());
		partnerToUpdate.setAdress(partner.getAdress());
		partnerToUpdate.setCity(partner.getCity());
	
		return partnerDao.save(partnerToUpdate);

	}

	public void delete(Partner partner) { partnerDao.delete(partner); }

	public Iterable<Partner> list() { return partnerDao.findAll(); }

	public Partner listId(Long id) {
		Optional<Partner> partnerOptional = partnerDao.findById(id);
		Partner partner = partnerOptional.orElseThrow(() -> new DataIntegrityViolationException("No se ha encontrado el registro de Partner"));
		partner.getSlogan().addAll(sloganDao.findByPartner(partner));
		return partner;
	}
	
}
