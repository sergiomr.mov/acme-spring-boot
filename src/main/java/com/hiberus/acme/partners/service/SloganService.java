package com.hiberus.acme.partners.service;

import java.util.List;
import java.util.Optional;

import com.hiberus.acme.partners.dao.PartnerDao;
import com.hiberus.acme.partners.dao.SloganDao;
import com.hiberus.acme.partners.model.Partner;
import com.hiberus.acme.partners.model.Slogan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class SloganService {

	@Autowired
	private SloganDao sloganDao;

	@Autowired
	private PartnerDao partnerDao;

	public Slogan save(Slogan slogan, Long id) { 

		Optional<Partner> partnerOptional = partnerDao.findById(id);
		Partner partner = partnerOptional.orElseThrow(() -> new DataIntegrityViolationException("No se ha encontrado el registro de Partner"));

		Long sloganCount = this.findByPartner(partner).stream().count();

		if(sloganCount >= 3){
			throw new DataIntegrityViolationException("Se ha alcanzado el máximo número de Slogans");
		}

		slogan.setPartner(partner);
		return sloganDao.save(slogan);

	}

	public void delete(Long id) { 
		Optional<Slogan> sloganOptional = sloganDao.findById(id);
		Slogan slogan = sloganOptional.orElseThrow(() -> new DataIntegrityViolationException("No se ha encontrado el registro de Slogan"));
		sloganDao.delete(slogan);
	}

	public List<Slogan> findByPartner(Partner partner){
		return sloganDao.findByPartner(partner);
	}

}